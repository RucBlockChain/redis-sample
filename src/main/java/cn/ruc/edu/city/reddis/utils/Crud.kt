package cn.ruc.edu.city.reddis.utils

//https://blog.csdn.net/dayuiicghaid/article/details/125680456
//https://javadoc.io/doc/redis.clients/jedis/latest/index.html
fun stringManipulate() {
    val client = ConnectUtils.getClient()

    // clear all the data
    client.flushDB()

    // set a key-value
    client.set("username", "peter")

    // get a key-value
    println(client.get("username"))

    // whether the key exist
    println(client.exists("username"))

    // rename the key
    client.rename("username", "username1")

    println(client.select(0))

    // get the db size
    println(client.dbSize())

    // flush all the data
    client.flushAll()
    println(client.dbSize()) //0

    // delete the spec key
    client.del("username1")

    // append the data in a spec key
    client.set("key3", "hello ")
    client.append("key3", " world")
    println(client.get("key3"))

    // multi manipulation
    client.mset("key1", "value1", "key2", "value2")
    println(client.mget("key1", "key2"))
    client.del("key1", "key2")

    client.flushDB()

    // set the key-value if key not exist
    client.setnx("key1", "value1")
}


fun listManipulate() {

    println("======listManipulate==========")
    val client = ConnectUtils.getClient()

    // remove all the data
    client.flushDB()

    // push the data
    client.lpush("collections", "A", "B", "C", "D", "E", "F")
    client.lpush("collections", "G")

    println(client.lindex("collections", 1)) // get the top stack value
    // so means to G


    // get the list from the range
    println(client.lrange("collections", 0, -1))

    // delete the spec item range values
    client.ltrim("collections", 0, 3)
    println(client.lrange("collections", 0, -1))

    // left handed pop up a value
    println(client.lpop("collections"))
    println(client.lrange("collections", 0, -1))

    // get the length
    println(client.llen("collections"))

    // right-handed pop
    println(client.rpop("collections"))
}


fun setManipulate() {
    println("======setManipulate==========")
    val client = ConnectUtils.getClient()

    client.flushDB()

    // add the value to the set
    client.sadd("sets", "e1", "e2", "e3", "e4", "e5", "e6")

    // get all values store in that set
    println(client.smembers("sets"))

    // remove the spec values
    client.srem("sets", "e1", "e2")

    // random delete one value in the sets
    client.spop("sets")

    // get the count in the set
    println(client.smembers("sets"))

    // whether the value is in the set
    println(client.sismember("sets", "e4"))

    // move a value from a set to another
    client.smove("sets1", "sets1", "e4")

    // 交-并-差
    client.sinter("sets1", "sets2")
    client.sunion("sets1", "sets2")
    client.sdiff("sets1", "sets2")
}

fun hashManipulate() {
    println("======hashManipulate==========")
    val client = ConnectUtils.getClient()

    client.flushDB()

    val m = hashMapOf<String, String>()
    m["key1"] = "value1"
    m["key2"] = "value2"
    m["key3"] = "value3"
    m["key4"] = "value4"

    // set the hash
    client.hmset("xa", m)
    client.hset("xa1", m)

    // get the data
    println(client.hkeys("xa"))
    println(client.hvals("xa"))
    println(client.hgetAll("xa"))
    println(client.hget("xa", "key1"))
    println(client.hmget("xa", "key1", "key2"))

    // delete the data
    client.hdel("xa", "key1")

    // whether the key exists
    println(client.hexists("xa", "key1"))

    // execute the len
    println(client.hlen("xa"))
}


fun main() {
    stringManipulate()
    listManipulate()
    setManipulate()
    hashManipulate()
}