
package cn.ruc.edu.city.reddis.utils;

import redis.clients.jedis.Jedis;

public class ConnectUtils {

    public static Jedis getClient() {
        // 1. 创建Jedis对象
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        jedis.auth("password");
        return jedis;
    }
}
