#!/usr/bin/env bash

systemctl start docker

sudo docker pull redis

sudo docker run -d --name redis -p 6379:6379 redis --requirepass "password"