package cn.ruc.edu.city;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.sync.RedisCommands;

/**
 * https://zhuanlan.zhihu.com/p/143044609
 */
public class RedisImport {

    public static void main(String[] args) {

        String csvFile = "./csv/uscities.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        RedisURI url = RedisURI.create("redis://127.0.0.1:6379");
        url.setPassword("password");
        RedisClient redisClient = RedisClient.create(url);
        var connection = redisClient.connect();

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                String[] city = line.split(cvsSplitBy);


                RedisCommands<String, String> commands = connection.sync();//开启同步操作
                String key = city[0].replace("\"", "");
                String value = city[8].replace("\"", "");

                System.out.println("key: " + key + ", value:" + value);
                commands.set(key, value);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        redisClient.shutdown();

    }

}