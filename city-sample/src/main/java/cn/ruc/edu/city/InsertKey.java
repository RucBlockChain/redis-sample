package cn.ruc.edu.city;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisFuture;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;
import com.lambdaworks.redis.api.sync.RedisCommands;

import java.util.concurrent.ExecutionException;

public class InsertKey {

    public static void main(String[] args) {
        RedisURI url = RedisURI.create("redis://127.0.0.1:6379");
        url.setPassword("password");
        RedisClient redisClient = RedisClient.create(url);


        StatefulRedisConnection<String, String> connect = redisClient.connect();

        /**
         * 同步调用
         */
        RedisCommands<String, String> commands = connect.sync();//开启同步操作
        commands.set("hello", "hello world");
        String str = commands.get("hello");
        //启动redis命令和jedis一样,都是命令的名字
        System.out.println(str);
        /**
         * 异步调用
         */
        RedisAsyncCommands<String, String> asyncCommands = connect.async();//开启异步操作
        RedisFuture<String> future = asyncCommands.get("hello");
        try {
            String str1 = future.get();
            System.out.println(str1);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        connect.close();
        redisClient.shutdown();
    }

}
