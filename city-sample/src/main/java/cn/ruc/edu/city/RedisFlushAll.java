package cn.ruc.edu.city;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisURI;

public class RedisFlushAll {

	public static void main(String[] args) {
		RedisURI url = RedisURI.create("redis://127.0.0.1:6379");
		url.setPassword("password");
		RedisClient redisClient = RedisClient.create(url);

		var connection = redisClient.connect();

		connection.flushCommands();
		connection.close();
		redisClient.shutdown();

	}

}
